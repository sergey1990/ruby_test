class CreateTables < ActiveRecord::Migration
  def change
    create_table :banknotes, :id => false do |t|
      t.integer :nominal, null: false
      t.string :description
    end
    execute "ALTER TABLE banknotes ADD PRIMARY KEY (nominal);"

    create_table :operations do |t|
      t.integer :sum, null: false
      t.string :action, null: false
      t.datetime :created_at, null: false
    end

    create_table :banknote_amounts do |t|
      t.integer :nominal, null: false
      t.integer :count, null: false
    end

    add_foreign_key :banknote_amounts, :banknotes, column: :nominal, primary_key: "nominal"
  end
end
