Banknote.create! nominal: 1, description: ''
Banknote.create! nominal: 7, description: ''
Banknote.create! nominal: 11, description: ''
Banknote.create! nominal: 29, description: ''
Banknote.create! nominal: 50, description: ''
Banknote.create! nominal: 90, description: ''
Banknote.create! nominal: 110, description: ''

BanknoteAmount.create! nominal: 1, count: 0
BanknoteAmount.create! nominal: 7, count: 0
BanknoteAmount.create! nominal: 11, count: 0
BanknoteAmount.create! nominal: 29, count: 0
BanknoteAmount.create! nominal: 50, count: 20
BanknoteAmount.create! nominal: 90, count: 0
BanknoteAmount.create! nominal: 110, count: 0
