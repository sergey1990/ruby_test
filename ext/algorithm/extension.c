#include "ruby/ruby.h"

static VALUE withdrawal(VALUE self, VALUE banknotes, VALUE limits, VALUE sum) {
    Check_Type(banknotes, T_ARRAY);
    Check_Type(limits, T_ARRAY);
    Check_Type(sum, T_FIXNUM);

    int len = RARRAY_LEN(banknotes);
    int b[len];
    int l[len];
    int s = NUM2INT(sum);
    int start_limit[len];
    int i,j, k;

    for (i = 0; i < len; i++)
    {
        b[i] = NUM2INT(rb_ary_entry(banknotes, i));
        l[i] = NUM2INT(rb_ary_entry(limits, i));
        start_limit[i] = 0;
    }

    int f[s+1];
    int ls[s+1][len];

    for (i = 0; i <= s; i++)
    {
        f[i] = 0;
        memcpy(ls[i], start_limit, sizeof(int) * len);

        if (i == 0) {
            continue;
        }

        int is_exists = 0;
        for (j = 0; j < len; j++)
        {
            if (i >= b[j] && (f[i -b[j]] < f[i] || f[i] == 0)) {
                if (ls[i - b[j]][j] + 1 <= l[j]) {
                    for (k = 0; k < len; k++) {
                        ls[i][k] = ls[i - b[j]][k];
                    }

       		        is_exists =1;
                    ls[i][j] += 1;
                    f[i] = f[i - b[j]] + 1;
                }
            }

            if (is_exists == 0) {
                for (k = 0; k < len; k++) {
                    ls[i][k] = l[k] + 1;
                }
            }
        }
    }

    if (f[s] == 0) {
        return Qnil;
    }

    VALUE result = rb_ary_new();
    for (i = 0; i < len; i++)
    {
        rb_ary_push(result, INT2NUM(ls[s][i]));
    }

    return result;
}

void Init_extension(void) {
    VALUE Algorithm = rb_define_module("Algorithm");
    VALUE Atm = rb_define_class_under(Algorithm, "A", rb_cObject);
    rb_define_singleton_method(Atm, "withdrawal", withdrawal, 3);
}