
## How to install with docker

```
$ docker-compose up -d
```    
And app will be available by next address: localhost:3011
    
## Api docs

U Can use public swagger UI as example: https://petstore.swagger.io/
and use http://localhost:3011/api/swagger_doc as data

you just need to download it and open in any browser

## Tests

the command to configure test db
```
docker-compose exec atm-backend bundle exec rake spec RACK_ENV=test  db:create db:migrate db:seed
```

run tests
```
docker-compose exec atm-backend bundle exec rspec
```