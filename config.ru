require './config/application'

use OTR::ActiveRecord::ConnectionManagement

use Rack::Cors do
  allow do
    origins '*'
    resource '*', headers: :any, methods: :put
  end
end

module API
  class BaseController < Grape::API
    format :json
    prefix :api
    mount Atm::V1::AtmController
    add_swagger_documentation hide_documentation_path: false,
      schemes: ["http"],
      host: lambda { |request| "localhost:3011" },
      api_version: 'v1',
      info: {
        title: 'Atm',
        description: 'test app'
      }
  end
end

app = Rack::Builder.new do
  map '/' do
    run API::BaseController
  end
end

run app