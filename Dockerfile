FROM ruby:2.6.8-slim-buster

RUN apt-get update && apt-get -y upgrade && apt-get -y install build-essential
RUN apt-get install -y rubygems
RUN apt-get -y install default-mysql-client
RUN apt-get -y install default-libmysqlclient-dev

RUN /bin/sh -c "mkdir -p /usr/src/app"
WORKDIR /usr/src/app

COPY Gemfile /usr/src/app/
#COPY Gemfile.lock /usr/src/app/
RUN bundle install

COPY . /usr/src/app
RUN bundle exec rake compile