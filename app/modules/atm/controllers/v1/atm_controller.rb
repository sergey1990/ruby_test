module Atm
  module V1
    class AtmController < Grape::API
      version 'v1', using: :path
      desc 'withdraw banknotes' do
        success model: Entities::BanknoteEntity, code: 200, examples: { 'application/json' => { banknotes: [{ nominal: 50, count: 5 }] } }
        failure [{ model: Entities::BanknoteEntity, code: 400, examples: { 'application/json' => { error: 'One-time withdrawal amount exceeded the limit, amount exceeded the limit' } } }]
      end
      params do
        requires :sum, type: Integer, withdrawal_limit: 10000, amount_limit: true, documentation: { param_type: 'body' }
      end
      put :withdraw do
        banknotes = OperationService.withdrawal(params[:sum])

        if banknotes.nil?
          error!({ error: 'impossible to withdraw. No needed banknotes.' }, 400)
        end

        present banknotes, with: Entities::BanknoteEntity
      end

      desc 'charge atm' do
        success model: Entities::BanknoteEntity, code: 200, examples: { 'application/json' => { banknotes: [{ nominal: 50, count: 5 }] } }
        failure [{ model: Entities::BanknoteEntity, code: 400, examples: { 'application/json' => { error: '50 is not accepted by atm, athe banknote slot limit is exceeded' } } }]
      end
      params do
        requires :banknotes, type: Array, nominal: true, documentation: { param_type: 'body' } do
          requires :nominal, type: Integer
          requires :count, type: Integer
        end
      end
      put :charge do
        sum = params['banknotes'].reduce(0) { |count, item| count + item['nominal'] * item['count'] }
        puts params['banknotes']
        banknotes = OperationService.charge(sum, params['banknotes'])
        present banknotes, with: Entities::BanknoteEntity
      end
    end
  end
end