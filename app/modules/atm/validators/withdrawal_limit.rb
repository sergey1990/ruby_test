class WithdrawalLimit < Grape::Validations::Base
  def validate_param!(attr_name, params)
    limit = @option
    if (params[attr_name] > limit)
      raise Grape::Exceptions::Validation, params: [''], message: "One-time withdrawal amount exceeded the limit (max #{limit})"
    end
  end
end