class AmountLimit < Grape::Validations::Base
  def validate_param!(attr_name, params)
    curr_sum = BanknoteAmount.all.reduce(0) { |sum, item| sum + item.nominal * item.count }
    if curr_sum < params[attr_name]
      raise Grape::Exceptions::Validation, params: [''], message: "amount exceeded the limit (max #{curr_sum})"
    end
  end
end