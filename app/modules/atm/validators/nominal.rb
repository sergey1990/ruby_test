class Nominal < Grape::Validations::Base
  def validate_param!(attr_name, params)
    banknotes = Banknote.all.map do |item|
      item.nominal
    end

    params[attr_name].each do |param|
      next unless param.is_a?(Hash)
      next unless param.has_key?('nominal')

      unless banknotes.include?(param['nominal'])
        raise Grape::Exceptions::Validation, params: [@scope.full_name(attr_name)], message: " (#{param['nominal']}) is not accepted by atm"
      end
    end
  end
end