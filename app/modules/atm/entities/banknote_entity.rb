module Entities
  class BanknoteEntity < Grape::Entity
    expose :nominal
    expose :count
  end
end