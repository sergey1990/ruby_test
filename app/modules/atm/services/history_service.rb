class HistoryService
  def self.write sum, action
    Operation.create({ :sum => sum, :action => action, :created_at => DateTime.now })
  end
end

