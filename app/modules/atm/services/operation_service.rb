require './ext/algorithm/extension.so'

class OperationService

  # Withdrawal operation.
  def self.withdrawal sum
    banknotes = BanknoteAmount.lock("FOR UPDATE").all

    banknotes_arr = banknotes.map { |item| item.nominal }
    limit_arr = banknotes.map { |item| item.count }

    needed_banknotes_arr = Algorithm::A.withdrawal(banknotes_arr, limit_arr, sum)

    if needed_banknotes_arr.nil?
      return nil
    end

    needed_banknotes = []
    HistoryService.write(sum, 'withdrawal')
    banknotes.each_with_index do |banknote, index|
      banknote.update(count: banknote.count - needed_banknotes_arr[index])

      if needed_banknotes_arr[index] > 0
        needed_banknote = BanknoteAmount.new
        needed_banknote.nominal = banknote.nominal
        needed_banknote.count = needed_banknotes_arr[index]
        needed_banknotes.push(needed_banknote)
      end
    end

    needed_banknotes
  end

  # Charge operation.
  def self.charge sum, banknote_items
    banknotes = BanknoteAmount
                  .lock("FOR UPDATE")
                  .all

    exist_banknotes_hash = banknote_items.reduce({}) do |items, item|
      items[item['nominal']] = item['count']
      items
    end

    HistoryService.write(sum, 'charge')
    banknotes.each do |banknote|
      banknote.update(count: banknote.count + exist_banknotes_hash.fetch(banknote.nominal, 0))
    end

    banknotes
  end
end

