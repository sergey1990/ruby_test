require './ext/algorithm/extension.so'

describe 'Atm withdrawal operation' do
  include Rack::Test::Methods

  context 'withdrawal operation' do
    it 'operation 1' do
       banknotes = Algorithm::A.withdrawal([1, 2, 3, 27, 45], [0,3,0,2,2], 74)
       expect(banknotes).to eq([0, 1, 0, 1, 1])
    end

    it 'operation 2' do
       banknotes = Algorithm::A.withdrawal([1, 50, 90], [0,0,1], 100)
       expect(banknotes).to eq(nil)
    end

    it 'operation 3' do
       banknotes = Algorithm::A.withdrawal([1, 50, 90], [10,2,1], 100)
       expect(banknotes).to eq([0, 2, 0])
    end

    it 'operation 4' do
      banknotes = Algorithm::A.withdrawal([25, 50], [4,3], 200)
      expect(banknotes).to eq([2, 3])
    end

    it 'operation 5' do
       banknotes = Algorithm::A.withdrawal([25, 50], [4,3], 100)
       expect(banknotes).to eq([0, 2])
    end
  end
end