require 'spec_helper'

describe Atm::V1::AtmController do
  include Rack::Test::Methods

  def app
    Atm::V1::AtmController
  end

  context 'PUT /api/v1/charge' do
      subject(:request) { put '/v1/charge', params, 'CONTENT_TYPE' => 'application/json' }

      before { request }

      context 'when charge fails' do
        let(:params) { { banknotes: [{nominal: 25, count: 45 }] }.to_json }

        it 'responds with 400 status' do
          expect(last_response.status).to eq 400
        end
      end

      context 'when charge success' do
        let(:params) { {  banknotes: [{ nominal: 7, count: 35 }] }.to_json }

        it 'responds with 200 status' do
          expect(last_response.status).to eq 200
      end
    end
    end


  context 'PUT /api/v1/withdraw' do
    subject(:request) { put '/v1/withdraw', params, 'CONTENT_TYPE' => 'application/json' }

    before { request }

    context 'when withdraw fails' do
      let(:params) { { sum: 650 }.to_json }

      it 'responds with 400 status' do
        expect(last_response.status).to eq 400
      end
    end

    context 'when withdraw success' do
      let(:params) { { sum: 50 }.to_json }

      it 'responds with 200 status' do
        expect(last_response.status).to eq 200
      end
    end
  end
end