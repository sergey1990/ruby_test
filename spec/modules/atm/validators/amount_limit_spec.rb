describe 'AmountLimit' do
  include Rack::Test::Methods

   before do
      b = double("BanknoteAmount")
      allow(b).to receive(:nominal).and_return(20)
      allow(b).to receive(:count).and_return(10)
      allow(BanknoteAmount).to receive(:all) { [b] }
    end

  class AmountValidationTestController < Grape::API
    default_format :json
      params do
        requires :sum, type: Integer, amount_limit: true
      end
      put   do
      end
   end

  def app
    AmountValidationTestController
  end

  context 'sum limit' do
    it 'refuses wrong sum' do
      put '/',  { sum: 300 }.to_json, { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
      expect(last_response.status).to eq(400)
      expect(last_response.body).to eq('{"error":" amount exceeded the limit (max 200)"}')
    end

    it 'accepts valid sum' do
      put '/',  { sum: 150 }.to_json, { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
      expect(last_response.status).to eq(200)
    end
  end
end