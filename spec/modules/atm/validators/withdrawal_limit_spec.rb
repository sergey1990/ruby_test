describe 'Withdrawal Limit Validator' do
  include Rack::Test::Methods

  class WithdrawalLimitValidationTestController < Grape::API
    default_format :json
    params do
            requires :sum, type: Integer, withdrawal_limit: 100
    end
    put   do
    end
   end

  def app
    WithdrawalLimitValidationTestController
  end

  context 'withdrawal validation' do
    it 'success withdrawal limit validation' do
      put '/',  { sum: 50 }.to_json, { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
      expect(last_response.status).to eq(200)
    end

    it 'failed withdrawal limit validation' do
      put '/',  { sum: 250 }.to_json, { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
      expect(last_response.status).to eq(400)
      expect(last_response.body).to eq('{"error":" One-time withdrawal amount exceeded the limit (max 100)"}')
    end
  end
end