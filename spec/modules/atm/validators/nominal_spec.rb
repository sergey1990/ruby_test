describe 'Nominal Validator' do
  include Rack::Test::Methods

   before do
      b1 = double("Banknote")
      allow(b1).to receive(:nominal).and_return(5)
      allow(b1).to receive(:description).and_return('')

      b2 = double("Banknote")
      allow(b2).to receive(:nominal).and_return(20)
      allow(b2).to receive(:description).and_return('')
      allow(Banknote).to receive(:all) { [b1, b2] }
    end
  class NominalValidationTestController < Grape::API
    default_format :json
      params  do
        requires :banknotes, type: Array, nominal: true do
          requires :nominal, type: Integer
          requires :count, type: Integer
        end
      end
      put   do
      end
   end

  def app
    NominalValidationTestController
  end

  context 'nominal validation' do
    it 'success nominal validation' do
      put '/',  { banknotes: [{nominal: 5, count: 10}, {nominal: 20, count: 5}] }.to_json, { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
      expect(last_response.status).to eq(200)
    end

    it 'failed nominal validation' do
      put '/',  { banknotes: [{nominal: 2, count: 5}, {nominal: 20, count: 5}] }.to_json, { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
      expect(last_response.status).to eq(400)
      expect(last_response.body).to eq('{"error":"banknotes  (2) is not accepted by atm"}')
    end
  end
end